package emergencia.gestionAfiliado;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import emergencia.gestionPago.Factura;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Afiliado extends Persona {
    private String nroAfiliado;
    private String tipoPlan;
    private LocalDate fechaAfiliacion;
    private List<Pariente>grupoFamiliar;
    private static int contador = 1;

    public Afiliado(String nroAfiliado, String tipoPlan, String nombre, LocalDate fechaDeNacimiento, String dni, String numeroDeTelefono) {
        super(nombre, fechaDeNacimiento, dni, numeroDeTelefono);
        this.nroAfiliado = nroAfiliado;
        this.tipoPlan = tipoPlan;
        this.fechaAfiliacion = LocalDate.now(); 
        this.grupoFamiliar = new ArrayList<Pariente>();
        
    }

    public static void aumentarContador(){
        contador += 1;
    }
    public static int getContador() {
        return contador;
    }
    public void setNroAfiliado(String nroAfiliado) {
        this.nroAfiliado = nroAfiliado;
    }

    public void setTipoPlan(String tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public void setFechaAfiliacion(LocalDate fechaAfiliacion) {
        this.fechaAfiliacion = fechaAfiliacion;
    }

    public void setGrupoFamiliar(List<Pariente> grupoFamiliar) {
        this.grupoFamiliar = grupoFamiliar;
    }

    public String getNroAfiliado() {
        return nroAfiliado;
    }
    
    public String getTipoPlan() {
        return tipoPlan;
    }

    public LocalDate getFechaAfiliacion() {
        return fechaAfiliacion;
    }

    public List<Pariente> getGrupoFamiliar() {
        return grupoFamiliar;
    }
    


    public void agregarPariente(Pariente pariente) throws ElementoDuplicadoException {
        if (grupoFamiliar.contains(pariente)){
            throw new ElementoDuplicadoException("El pariente ya pertenece al registro.");   
        }
        else if (this.getDni().equals(pariente.getDni())){
            throw new ElementoDuplicadoException("Los datos ingresados corresponden al titular afiliado.");
        }
        else grupoFamiliar.add(pariente);
    }

    public Pariente buscarPariente(String dni) throws ElementoInexistenteException {
        for (Pariente aux : grupoFamiliar) {
            if (aux.getDni().equals(dni)) {
                return aux;
            }
        }
        throw new ElementoInexistenteException("El pariente no pudo ser encontrado.");
    }

    public void eliminarPariente(String dni) throws ElementoInexistenteException {
        Pariente pariente = buscarPariente(dni);
        grupoFamiliar.remove(pariente);
    }

    public void modificarPariente(String dni, Pariente parienteNuevo) throws ElementoDuplicadoException, ElementoInexistenteException {
        eliminarPariente(dni);
        agregarPariente(parienteNuevo);
    }
    
    public int getCantidadParientes(){
        return grupoFamiliar.size();
    }
}
