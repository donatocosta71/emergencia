/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.gestionAfiliado;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Camib
 */
public class RegistroAfiliado {
    private List<Afiliado> afiliados;
    
    public RegistroAfiliado(){
        afiliados=new ArrayList<Afiliado>();
    }
    
    public void darAlta(Afiliado afiliado)throws ElementoDuplicadoException{
        if(afiliados.contains(afiliado)){
            throw new ElementoDuplicadoException("El afiliado ya está agregado.");
        }
        else{
            afiliados.add(afiliado);
        }
    }
    
    public Afiliado buscaAfiliado(String nroAfiliado) throws ElementoInexistenteException {
        for(Afiliado afiliadoAux: afiliados){
            if(afiliadoAux.getNroAfiliado().equals(nroAfiliado)){
                return afiliadoAux;
            }
        }
        throw new ElementoInexistenteException(" No pudo ser encontrado.");
    }
    
    public void modificarAfiliado(String dni, Afiliado nuevoAf)throws ElementoInexistenteException, ElementoDuplicadoException{
        darBaja(dni);
        darAlta(nuevoAf);
    }
    
    public Persona buscarPersona(String dni, String nroAfiliado) throws ElementoInexistenteException{
        Afiliado afiliado = buscaAfiliado(nroAfiliado);
        if(afiliado.getDni().equals(dni)){
            return afiliado;
        }
        else{
            return afiliado.buscarPariente(dni);
        }
    }
    public void darBajaPersona(String dni, String nroAfiliado) throws ElementoInexistenteException {
        Afiliado afiliado = buscaAfiliado(nroAfiliado);
        if (afiliado.getDni().equals(dni)) {
            afiliados.remove(afiliado);
        } else {
            buscaAfiliado(nroAfiliado).eliminarPariente(dni);
        }
    }
    
    
    public void darBaja(String dni)throws ElementoInexistenteException{
        Afiliado afiliado= buscaAfiliado(dni);
        afiliados.remove(afiliado);
    }
}
