package emergencia.gestionAfiliado;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class HistoriaClinica {
    private String grupoSanguineo;
    private float altura;
    private float peso;
    private boolean diabetes;
    private boolean hipertension;
    private boolean hipotension;

    public HistoriaClinica(String grupoSanguineo, float altura, float peso, boolean diabetes, boolean hipertension, boolean hipotension) {
        this.grupoSanguineo = grupoSanguineo;
        this.altura = altura;
        this.peso = peso;
        this.diabetes = diabetes;
        this.hipertension = hipertension;
        this.hipotension = hipotension;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    public float getAltura() {
        return altura;
    }

    public float getPeso() {
        return peso;
    }

    public boolean isDiabetes() {
        return diabetes;
    }

    public boolean isHipertension() {
        return hipertension;
    }

    public boolean isHipotension() {
        return hipotension;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setDiabetes(boolean diabetes) {
        this.diabetes = diabetes;
    }

    public void setHipertension(boolean hipertension) {
        this.hipertension = hipertension;
    }

    public void setHipotension(boolean hipotension) {
        this.hipotension = hipotension;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HistoriaClinica other = (HistoriaClinica) obj;
        if (Float.floatToIntBits(this.altura) != Float.floatToIntBits(other.altura)) {
            return false;
        }
        if (Float.floatToIntBits(this.peso) != Float.floatToIntBits(other.peso)) {
            return false;
        }
        if (this.diabetes != other.diabetes) {
            return false;
        }
        if (this.hipertension != other.hipertension) {
            return false;
        }
        if (this.hipotension != other.hipotension) {
            return false;
        }
        return Objects.equals(this.grupoSanguineo, other.grupoSanguineo);
    }
    
    
}
