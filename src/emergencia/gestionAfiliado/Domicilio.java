package emergencia.gestionAfiliado;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Domicilio {
    private String calle;
    private String numeracion;
    private String piso;
    private String nroDepartamento;

    public Domicilio(String calle, String numeracion, String piso, String nroDepartamento) {
        this.calle = calle;
        this.numeracion = numeracion;
        this.piso = piso;
        this.nroDepartamento = nroDepartamento;
    }
    
    public String getCalle() {
        return calle;
    }

    public String getNumeracion() {
        return numeracion;
    }
    
    public String getPiso() {
        return piso;
    }

    public String getNroDepartamento() {
        return nroDepartamento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Domicilio other = (Domicilio) obj;
        if (!this.numeracion.equals(other.numeracion)) {
            return false;
        }
        if (!this.piso.equals(other.piso)) {
            return false;
        }
        if (!Objects.equals(this.calle, other.calle)) {
            return false;
        }
        return Objects.equals(this.nroDepartamento, other.nroDepartamento);
    }

}
