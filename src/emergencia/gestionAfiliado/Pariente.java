package emergencia.gestionAfiliado;
import java.time.LocalDate;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Pariente extends Persona {
    private String parentesco;
    private String nroAfiliado;
    

    public Pariente(String parentesco,String nroAfiliado, String nombre, LocalDate fechaDeNacimiento, String dni, String numeroDeTelefono) {
        super(nombre, fechaDeNacimiento, dni, numeroDeTelefono);
        this.parentesco = parentesco;
        this.nroAfiliado = nroAfiliado;
    }
    
    public String getParentesco() {
        return parentesco;
    }  
    public String getNroAfiliado() {
        return nroAfiliado;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public void setNroAfiliado(String nroAfiliado) {
        this.nroAfiliado = nroAfiliado;
    }
    
}
