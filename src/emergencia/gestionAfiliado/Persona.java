package emergencia.gestionAfiliado;


import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Persona {
    private String nombre;
    private LocalDate fechaDeNacimiento;
    private String dni;
    private String numeroDeTelefono;

    public Persona(String nombre, LocalDate fechaDeNacimiento, String dni, String numeroDeTelefono) {
        this.nombre = nombre;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.dni = dni;
        this.numeroDeTelefono = numeroDeTelefono;
    }
    
    public String getNombre() {
        return nombre;
    }

    public LocalDate getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public String getDni() {
        return dni;
    }

    public String getNumeroDeTelefono() {
        return numeroDeTelefono;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setNumeroDeTelefono(String numeroDeTelefono) {
        this.numeroDeTelefono = numeroDeTelefono;
    }
    
    public int getEdad(){
        return Period.between(fechaDeNacimiento, LocalDate.now()).getYears();
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        return this.dni.equals(other.dni);
    }    
}