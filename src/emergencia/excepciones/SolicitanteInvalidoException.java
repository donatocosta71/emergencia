/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.excepciones;

/**
 *
 * @author Camib
 */
public class SolicitanteInvalidoException extends Exception {

    public SolicitanteInvalidoException(String message) {
        super(message);
    }
}