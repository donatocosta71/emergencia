/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.excepciones;

/**
 *
 * @author Usuario
 */
public class ElementoInexistenteException extends Exception {

    public ElementoInexistenteException(String message) {
        super(message);
    }

}
