/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia;

import emergencia.gestionAfiliado.RegistroAfiliado;
import emergencia.gestionAsistencia.CarpetaSolicitudesMedicas;
import emergencia.gestionEmpleado.LegajoEmpleado;
import emergencia.gestionMovil.FlotaVehicular;
import emergencia.gestionPago.RegistroComprobantes;

/**
 *
 * @author Usuario
 */
public class Empresa { //   PATRON SINGLETON

    private static Empresa empresa;
    private RegistroAfiliado regAfiliado;
    private CarpetaSolicitudesMedicas carpetaSolicitudesMedicas;
    private LegajoEmpleado legajoEmpleado;
    private FlotaVehicular flotaVehicular;
    private RegistroComprobantes regComprobantes;

    private Empresa() {
        regAfiliado = new RegistroAfiliado();
        carpetaSolicitudesMedicas = new CarpetaSolicitudesMedicas();
        legajoEmpleado = new LegajoEmpleado();
        flotaVehicular = new FlotaVehicular();
        regComprobantes = new RegistroComprobantes();

    }

    public static Empresa instancia() {
        if (empresa == null) {
            empresa = new Empresa();
        }
        return empresa;
    }

    public RegistroAfiliado getRegAfiliado() {
        return regAfiliado;
    }

    public CarpetaSolicitudesMedicas getCarpetaSolicitudesMedicas() {
        return carpetaSolicitudesMedicas;
    }

    public LegajoEmpleado getLegajoEmpleado() {
        return legajoEmpleado;
    }

    public FlotaVehicular getFlotaVehicular() {
        return flotaVehicular;
    }

    public RegistroComprobantes getRegComprobantes() {
        return regComprobantes;
    }
}