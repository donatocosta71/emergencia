/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.gestionAsistencia;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class CarpetaSolicitudesMedicas {

    private List<Solicitud> regSolicitudes;
    

    public CarpetaSolicitudesMedicas() {
        regSolicitudes = new ArrayList<Solicitud>();
    }



    public void darAlta(Solicitud soli) throws ElementoDuplicadoException {
        if (regSolicitudes.contains(soli)) {
            throw new ElementoDuplicadoException("SOLICITUD YA EMITIDA.");
        }
        regSolicitudes.add(soli);
    }

    public Solicitud buscaSolicitud(LocalDate fecha, String dni) throws ElementoInexistenteException {
        for (Solicitud aux : regSolicitudes) {
            if (aux.getFecha().equals(fecha) && aux.getSolicitante().getDni().equals(dni)) {
                return aux;
            }
        }
        throw new ElementoInexistenteException("LOS DATOS INGRESADOS NO CORRESPONDEN A NINGUNA ASISTENCIA MÉDICA.");
    }
    
    public List<Solicitud> buscaSolicitudesPeriodo(LocalDate fechaDesde, LocalDate fechaHasta)throws ElementoInexistenteException{
        List<Solicitud> solicitudesPeriodo = new ArrayList<Solicitud>();
        Boolean encontrado= false;
        for(Solicitud aux : regSolicitudes ){
            if(aux.getFecha().isAfter(fechaDesde)&& aux.getFecha().isBefore(fechaHasta)){
                solicitudesPeriodo.add(aux);
                encontrado = true;
            }
        }
        if(encontrado){
           return solicitudesPeriodo;
        }
        throw new ElementoInexistenteException("NO SE HA ENCONTRADO NINGUNA ASISTENCIA MEDICA EN EL RANGO DE FECHA.");
    }
    
    public void darBaja (Solicitud solicitud) {
        
        regSolicitudes.remove(solicitud);
    }
    
    public void mofificarSolicitud (Solicitud viejaSolicitud, Solicitud newSoli) throws ElementoDuplicadoException{
        darBaja (viejaSolicitud);
        darAlta (newSoli);
    }
}
