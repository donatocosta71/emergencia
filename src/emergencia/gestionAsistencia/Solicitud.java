package emergencia.gestionAsistencia;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import emergencia.gestionAfiliado.Domicilio;
import emergencia.gestionAfiliado.Persona;
import emergencia.gestionMovil.Movil;
import emergencia.gestionEmpleado.Medico;
import emergencia.gestionEmpleado.Enfermero;
import emergencia.gestionEmpleado.Chofer;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Usuario
 */
public class Solicitud {
    private LocalDate fecha;
    private Persona solicitante;
    private Domicilio domicilio;
    private String motivo;
    private Movil movil;
    private Chofer chofer;
    private Medico medico;
    private Enfermero enfermero;
    private String diagnostico;
    private String tipoAtencion;

    public Solicitud(Persona solicitante, Domicilio domicilio, String motivo, Movil movil, Chofer chofer, Medico medico, Enfermero enfermero) {
        this.solicitante = solicitante;
        this.domicilio = domicilio;
        this.motivo = motivo;
        this.movil = movil;
        this.chofer = chofer;
        this.medico = medico;
        this.enfermero = enfermero;
        fecha = LocalDate.now();
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }
    
    public LocalDate getFecha() {
        return fecha;
    }

    public Persona getSolicitante() {
        return solicitante;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public String getMotivo() {
        return motivo;
    }

    public Movil getMovil() {
        return movil;
    }

    public Chofer getChofer() {
        return chofer;
    }

    public Medico getMedico() {
        return medico;
    }

    public Enfermero getEnfermero() {
        return enfermero;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public String getTipoAtencion() {
        return tipoAtencion;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.fecha);
        hash = 29 * hash + Objects.hashCode(this.solicitante);
        hash = 29 * hash + Objects.hashCode(this.domicilio);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Solicitud other = (Solicitud) obj;
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (!Objects.equals(this.solicitante, other.solicitante)) {
            return false;
        }
        return Objects.equals(this.domicilio, other.domicilio);
    }
}
