package emergencia.gestionEmpleado;


import emergencia.gestionEmpleado.Empleado;
import emergencia.gestionAfiliado.Domicilio;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Chofer extends Empleado {
    private String nroLicencia;
    private String tipoLicencia;

    public Chofer(String nroLicencia, String tipoLicencia, String cuil, String nombre, LocalDate fechaDeNacimiento, String dni, String numeroDeTelefono) {
        super(cuil, nombre, fechaDeNacimiento, dni, numeroDeTelefono);
        this.nroLicencia = nroLicencia;
        this.tipoLicencia = tipoLicencia;
    }

    public String getNroLicencia() {
        return nroLicencia;
    }

    public String getTipoLicencia() {
        return tipoLicencia;
    }

    public void setTipoLicencia(String tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.nroLicencia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Chofer other = (Chofer) obj;
        return Objects.equals(this.nroLicencia, other.nroLicencia);
    }

}
