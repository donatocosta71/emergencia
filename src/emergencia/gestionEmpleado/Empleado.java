package emergencia.gestionEmpleado;


import emergencia.gestionAfiliado.Domicilio;
import emergencia.gestionAfiliado.Persona;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;




/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 * 
 * @author Camib
 */
public class Empleado extends Persona {
    private LocalDate fechaAlta;
    private String cuil;

    public Empleado( String cuil, String nombre, LocalDate fechaDeNacimiento, String dni,  String numeroDeTelefono) {
        super(nombre, fechaDeNacimiento, dni, numeroDeTelefono);
        this.fechaAlta = LocalDate.now();
        this.cuil = cuil;
    }

    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    public String getCuil() {
        return cuil;
    }
    
    public int getAntiguedad(){
        return Period.between(fechaAlta, LocalDate.now()).getYears();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.cuil);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        return Objects.equals(this.cuil, other.cuil);
    }
}
