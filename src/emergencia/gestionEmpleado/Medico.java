package emergencia.gestionEmpleado;


import emergencia.gestionEmpleado.Empleado;
import emergencia.gestionAfiliado.Domicilio;
import java.time.LocalDate;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Medico extends Empleado {
    private String especialidad;
    private String matricula;

    public Medico(String especialidad, String matricula, String cuil, String nombre, LocalDate fechaDeNacimiento, String dni, String numeroDeTelefono) {
        super( cuil, nombre, fechaDeNacimiento, dni, numeroDeTelefono);
        this.especialidad = especialidad;
        this.matricula = matricula;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public String getMatricula() {
        return matricula;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Medico other = (Medico) obj;
        return this.matricula.equals(other.matricula);
    }  
}
