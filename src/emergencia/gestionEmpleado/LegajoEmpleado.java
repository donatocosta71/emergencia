/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.gestionEmpleado;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Camib
 */
public class LegajoEmpleado {
    private List<Empleado> empleados;
    
    public LegajoEmpleado(){
        empleados=new ArrayList<Empleado>();
    }
    
    public void darAlta(Empleado empleado)throws ElementoDuplicadoException{
        if(empleados.contains(empleado)){
            throw new ElementoDuplicadoException("Empleado ya dado de alta");
        }else{
            empleados.add(empleado);
        }
    }
    
    public Empleado buscaEmpleado(String cuil) throws ElementoInexistenteException {
        for(Empleado empleadoAux: empleados){
            if(empleadoAux.getCuil().equals(cuil)){
                return empleadoAux;
            }
        }
        throw new ElementoInexistenteException(cuil+" no encontrado.");
    }
    
    public void modificarEmpleado(String cuil, Empleado nuevoEmp)throws ElementoInexistenteException, ElementoDuplicadoException{
        darBaja(cuil);
        darAlta(nuevoEmp);
    }
    
    public void darBaja(String cuil)throws ElementoInexistenteException{
        Empleado empleado= buscaEmpleado(cuil);
        empleados.remove(empleado);
    }
    
    public List<Medico> getMedicos(){
        List<Medico> medicos= new ArrayList<Medico>();
        for(Empleado medicoAux:empleados){
            if(medicoAux instanceof Medico){
                medicos.add((Medico) medicoAux);
            }
        }
        return medicos;
    }
    
    public List<Enfermero> getEnfermeros(){
        List<Enfermero> enfermeros= new ArrayList<Enfermero>();
        for(Empleado enfermeroAux: empleados){
            if(enfermeroAux instanceof Enfermero){
                enfermeros.add((Enfermero) enfermeroAux);
            }
        }
        return enfermeros;
    }
    
    public List<Chofer> getChoferes(){
        List<Chofer> choferes= new ArrayList<Chofer>();
        for(Empleado choferAux:empleados){
            if(choferAux instanceof Chofer){
                choferes.add((Chofer) choferAux);
            }
        }
        return choferes;
    }
    
}
