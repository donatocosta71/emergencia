package emergencia.gestionEmpleado;


import emergencia.gestionEmpleado.Empleado;
import emergencia.gestionAfiliado.Domicilio;
import java.time.LocalDate;


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Camib
 */
public class Administrativo extends Empleado {
    private String area;

    public Administrativo(String area, String cuil, String nombre, LocalDate fechaDeNacimiento, String dni, String numeroDeTelefono) {
        super(cuil, nombre, fechaDeNacimiento, dni, numeroDeTelefono);
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    
    
}
