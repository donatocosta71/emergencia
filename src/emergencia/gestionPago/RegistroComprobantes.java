/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.gestionPago;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import emergencia.gestionAfiliado.Afiliado;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class RegistroComprobantes {

    private List<Factura> regComprobantes;

    public RegistroComprobantes() {
        regComprobantes = new ArrayList<Factura>();
    }

    public List<Factura> getRegFacturas() {
        return regComprobantes;
    }

    public void emitirFactura(Factura factura) {
        if (regComprobantes.contains(factura)) {
            
        } else {
            regComprobantes.add(factura);
        }
    }

    public Factura buscaFactura(LocalDate fecha, String nroFactura) throws ElementoInexistenteException {
        for (Factura facturaAux : regComprobantes) {
            if (facturaAux.getFechaEmision().equals(fecha) && facturaAux.getNroFactura().equals(nroFactura)) {
                return facturaAux;
            }
        }
        throw new ElementoInexistenteException(" La factura no pudo ser encontrada.");
    }
    
    public List<Factura> buscaFacturasAfiliado(Afiliado afiliado) throws ElementoInexistenteException {
        boolean encontrado=false;
        List facturas = new ArrayList<Factura>();
        for (Factura facturaAux : regComprobantes) {
            if (facturaAux.getTitularAfiliado().equals(afiliado)) {
                encontrado=true;
                facturas.add(facturaAux);
            }
        }
        if(encontrado){
            return facturas;
        }
        throw new ElementoInexistenteException("No hay facturas a nombre de "+afiliado.getNombre()+"n° de Afiliado: "+afiliado.getNroAfiliado());
    }
    
    public List buscaFacturasAfiliadoPeriodo(Afiliado afiliado, LocalDate fechaDesde, LocalDate fechaHasta) throws ElementoInexistenteException {
        boolean encontrado=false;
        List facturas = new ArrayList<Factura>();
        for (Factura facturaAux : regComprobantes) {
            if (facturaAux.getTitularAfiliado().equals(afiliado) && facturaAux.getFechaEmision().isAfter(fechaDesde) && facturaAux.getFechaEmision().isBefore(fechaHasta)) 
            {
                encontrado=true;
                facturas.add(facturaAux);
            }
        }
        if(encontrado){
            return facturas;
        }
        throw new ElementoInexistenteException("No hay facturas a nombre de "+afiliado.getDni()+"en el rango de fecha.");
    }
    
    public List buscaFacturasPeriodo(LocalDate fechaDesde, LocalDate fechaHasta) throws ElementoInexistenteException {
        boolean encontrado=false;
        List facturas = new ArrayList<Factura>();
        for (Factura facturaAux : regComprobantes) {
            if (facturaAux.getFechaEmision().isAfter(fechaDesde) && facturaAux.getFechaEmision().isBefore(fechaHasta)) 
            {
                encontrado=true;
                facturas.add(facturaAux);
            }
        }
        if(encontrado){
            return facturas;
        }
        throw new ElementoInexistenteException("No hay facturas registradas en el rango de fecha.");
    }

    public void anularFactura(LocalDate fecha, String nroFactura) throws ElementoInexistenteException {
        Factura factura = buscaFactura(fecha, nroFactura);
        regComprobantes.remove(factura);
    }

    public void modificarFactura(LocalDate fecha, String nroFactura, Factura newFactura) throws ElementoInexistenteException, ElementoDuplicadoException {
        anularFactura(fecha, nroFactura);
        emitirFactura(newFactura);
    }
    
    public Factura ultimaFactura(Afiliado afiliado) throws ElementoInexistenteException {
        Factura facturaReciente = buscaFacturasAfiliado(afiliado).get(0);
        for (Factura facturaAux : buscaFacturasAfiliado(afiliado)) {
            if (facturaAux.getFechaEmision().isAfter(facturaReciente.getFechaEmision())) {
                facturaReciente = facturaAux;
            }
        }
        return facturaReciente;
    }

    public int mesesDeMora(Afiliado afiliado) throws ElementoInexistenteException {
        Factura facturaReciente = ultimaFactura(afiliado);
        Period periodo = Period.between(facturaReciente.getFechaEmision(), LocalDate.now());
        return (periodo.getYears() * 12 + periodo.getMonths());
    }

}