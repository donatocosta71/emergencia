package emergencia.gestionPago;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.gestionAfiliado.Afiliado;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.JOptionPane;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Usuario
 */
public class Factura {
    private static int contador = 1;
    private LocalDate fechaEmision;
    private String nroFactura;
    private Afiliado titularAfiliado;
    private List<Item> items;

    public Factura(LocalDate fechaEmision,String nroFactura, Afiliado titularAfiliado) {
        this.fechaEmision = fechaEmision;
        this.nroFactura= nroFactura;
        this.titularAfiliado = titularAfiliado;
        items = new ArrayList<Item>();
    }

    public static void decrementarContador(){
        contador -= 1;
    }
    
    public static void aumentarContador(){
        contador += 1;
    }
    public static int getContador() {
        return contador;
    }

    public List<Item> getItems() {
        return items;
    }
    
    public LocalDate getFechaEmision() {
        return fechaEmision;
    }

    public String getNroFactura() {
        return nroFactura;
    }
    
    public Afiliado getTitularAfiliado() {
        return titularAfiliado;
    }

    public Double totalNetoFactura(){
        Double total=0.0;
        for(Item itemAux : items){
            total += itemAux.totalNetoItem();
        }
        return total;
    }
    
    public Double totalBrutoFactura(){
        Double total=0.0;
        for(Item itemAux : items){
            total += itemAux.totalBrutoItem();
        }
        return total;
    }
    

    public void agregarItem(Item item)  {
        if (items.contains(item)) {
            for (Item aux : items){
                if (aux.equals(item)){
                    aux.setCantidad(aux.getCantidad() + item.getCantidad());
                    JOptionPane.showMessageDialog(null, "ITEM REPETIDO\nSe sumó una cantidad de "+item.getCantidad()+" al item "+aux.getServicio());
                }
            }
        } else {
            items.add(item);
        }
    }
    
    public void eliminarItem(int fila){
        items.remove(fila);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.fechaEmision);
        hash = 53 * hash + Objects.hashCode(this.nroFactura);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Factura other = (Factura) obj;
        if (!Objects.equals(this.nroFactura, other.nroFactura)) {
            return false;
        }
        return Objects.equals(this.fechaEmision, other.fechaEmision);
    }

 

    public class Item {

        private String servicio;
        private Double precioUnit;
        private Double iva;
        private int cantidad;

        public Item(String servicio, Double precioUnit, Double iva, int cantidad) {
            this.servicio = servicio;
            this.precioUnit = precioUnit;
            this.iva = iva;
            this.cantidad = cantidad;
        }
        
        
        public Double totalNetoItem(){
            
            Double total= (precioUnit*cantidad)+(precioUnit*cantidad)*(iva/100)  ;
            return total;
        }
        
        public Double totalBrutoItem(){
            
            Double total= (precioUnit*cantidad)+(precioUnit*cantidad)*(iva/100)  ;
            return total;
        }
        
        public int getCantidad() {
            return cantidad;
        }

        public String getServicio() {
            return servicio;
        }

        public Double getPrecioUnit() {
            return precioUnit;
        }

        public Double getIva() {
            return iva;
        }

        public void setCantidad(int cantidad) {
            this.cantidad = cantidad;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Item other = (Item) obj;
            if (!Objects.equals(this.servicio, other.servicio)) {
                return false;
            }
            if (!Objects.equals(this.precioUnit, other.precioUnit)) {
                return false;
            }
            return Objects.equals(this.iva, other.iva);
        }

        
    } //Fin clase interna Item.
}