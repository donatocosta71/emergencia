package emergencia.gestionMovil;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Camib
 */
public class Movil {

    private String marca;
    private String patente;
    private String modelo;
    private boolean ocupado;

    public Movil(String marca, String patente, String modelo) {
        this.marca = marca;
        this.patente = patente;
        this.modelo = modelo;
        ocupado= false;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }
    
    public String getMarca() {
        return marca;
    }

    public String getPlaca() {
        return patente;
    }

    public String getModelo() {
        return modelo;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movil other = (Movil) obj;
        return Objects.equals(this.patente, other.patente);
    }

} 