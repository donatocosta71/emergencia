/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.gestionMovil;

import emergencia.excepciones.ElementoDuplicadoException;
import emergencia.excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Camib
 */
public class FlotaVehicular {
    private List<Movil> moviles;
    public FlotaVehicular(){
        moviles=new ArrayList<Movil>();
    }

    public List<Movil> getMoviles() {
        return moviles;
    }
    
    public List<Movil> getMovilesLibres(){
        List<Movil> desocupados = new ArrayList<Movil>();
        for(Movil movilAux : moviles){
            if(!movilAux.isOcupado()){
                desocupados.add(movilAux);
            }
        }
        return desocupados;
    }
    
    public void darAlta(Movil movil)throws ElementoDuplicadoException{
        if(moviles.contains(movil)){
            throw new ElementoDuplicadoException("Movil ya dado de alta");
        }else{
            moviles.add(movil);
        }
    }
    
    public Movil buscaMovil(String placa) throws ElementoInexistenteException {
        for(Movil movilAux: moviles){
            if(movilAux.getPlaca().equals(placa)){
                return movilAux;
            }
        }
        throw new ElementoInexistenteException(placa+" NO ENCONTRADO.");
    }
    
    public void modificarMovil(Movil viejoMovil, Movil nuevoMovil)throws ElementoInexistenteException, ElementoDuplicadoException{
        darBaja(viejoMovil);
        darAlta(nuevoMovil);
    }
    
    public void darBaja(Movil movil){
        moviles.remove(movil);
    }
}
